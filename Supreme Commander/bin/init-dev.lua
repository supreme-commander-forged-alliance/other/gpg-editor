
path = {}

local function mount_dir(dir, mountpoint)
    table.insert(path, { dir = dir, mountpoint = mountpoint } )
end

local function mount_contents(dir, mountpoint)
    for _,entry in io.dir(dir .. '\\*') do
        if entry != '.' and entry != '..' then
            local mp = string.lower(entry)
            mp = string.gsub(mp, '[.]scd$', '')
            mp = string.gsub(mp, '[.]zip$', '')
            mount_dir(dir .. '\\' .. entry, mountpoint .. '/' .. mp)
        end
    end
end

-- Begin map mounting section
-- This section mounts movies and sounds from maps, essential for custom missions and scripted maps
local function mount_map_dir(dir, glob, mountpoint)
    LOG('mounting maps from: '..dir)
    mount_contents(dir, mountpoint)
    for _, map in io.dir(dir..glob) do
        for _, folder in io.dir(dir..'\\'..map..'\\**') do
            if folder == 'env' then
                LOG('Found env files in: '..map)
                mount_contents(dir..map..'\\env', '/')
            elseif folder == 'effects' then
                LOG('Found map effects in: '..map)
                mount_dir(dir..map..'\\effects', '/effects')
            elseif folder == 'movies' then
                LOG('Found map movies in: '..map)
                mount_dir(dir..map..'\\movies', '/movies')
            elseif folder == 'sounds' then
                LOG('Found map sounds in: '..map)
                mount_dir(dir..map..'\\sounds', '/sounds')
            end
        end
    end
end


-- load in maps from the custom maps folder of Supreme Commander
mount_contents(SHGetFolderPath('PERSONAL') .. '\\My Games\\Gas Powered Games\\Supreme Commander\\mods', '/mods')
mount_map_dir(SHGetFolderPath('PERSONAL') .. '\\My Games\\Gas Powered Games\\Supreme Commander\\maps\\', '**', '/maps')

-- load in maps from the installation folder
mount_contents(SHGetFolderPath('LOCAL_APPDATA') .. '\\Gas Powered Games\\Supreme Commander\\mods', '/mods')
mount_map_dir(SHGetFolderPath('LOCAL_APPDATA') .. '\\Gas Powered Games\\Supreme Commander\\maps\\', '**', '/maps')

-- load in maps from the custom maps folder of Forged Alliance
mount_contents(SHGetFolderPath('PERSONAL') .. '\\My Games\\Gas Powered Games\\Supreme Commander Forged Alliance\\mods', '/mods')
mount_map_dir(SHGetFolderPath('PERSONAL') .. '\\My Games\\Gas Powered Games\\Supreme Commander Forged Alliance\\maps\\', '**', '/maps')

-- load in maps from the installation folder
mount_contents(SHGetFolderPath('LOCAL_APPDATA') .. '\\Gas Powered Games\\Supreme Commander Forged Alliance\\mods', '/mods')
mount_map_dir(SHGetFolderPath('LOCAL_APPDATA') .. '\\Gas Powered Games\\Supreme Commander Forged Alliance\\maps\\', '**', '/maps')

-- load in typical scd files
mount_dir(InitFileDir .. '/../gamedata/*.scd', '/')

-- load in nomad files (also typical scd files)
mount_dir(InitFileDir .. '/../gamedata/*.nmd', '/')

-- load in ... everything?
mount_dir(InitFileDir .. '/..', '/')


hook = {
    '/schook'
}



protocols = {
    'http',
    'https',
    'mailto',
    'ventrilo',
    'teamspeak',
    'daap',
    'im',
}
