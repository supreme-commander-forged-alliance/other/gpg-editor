# GPG Editor

Contains all the information and tricks to get started with the old GPG editor.

## The 'old' editor

This is a repository about the old editor. The new editor can be found here:
 - https://github.com/ozonexo3/FAForeverMapEditor/releases
  
For more information about using this editor see the webpages folder for a cached tutorial. Some maps are stored in a newer format (v60) and the old editor can not open that format. Use the new editor to change the map to the old format (v56).

In order to use the editor you need to have `Supreme Commander` installed on your system. It should be drag and drop from that point onwards: merge the two folders and override any files.

Afer starting the editor at least once you can change the default window size and location. You can find the preference file here:
 - `C:\Users\%USER%\AppData\Local\Gas Powered Games\SupremeCommander`

Do not maximize the main view. The engine internally doesn't scale along with it - causing the graphics to look pixelated and the zoom to be a bit off. You can only load in maps from the installation folder.

### About the bin folder

 - `SCEditor.exe`: The executable that can load in maps of the old format (v56) and edit them.
 - `SCEditor.sh`: A script that calls the edtior with the right program arguments.
 - `init-dev.lua`: An adjusted initialisation file for 

### About the gamedata

Inside the gamedata folder is _everything_ that the game has. The `.scd` extension is an abbreviation for `Supreme Commander Data` and is essentially a `.zip`. You can change the extension and unpack it to see what is inside.

 - `cutsbrushes.scd`: A set of brushes to use for editing the heightmap.
 - `editor.scd`: A set of utility scripts used to populate the editor, such as wave and marker templates.
 - `mohodata.scd`: An adjusted version of the original mohodata to allow props to be loaded from any map, similar to the Ozone editor.
 - `Z_lua.scd`: Contains data about the units introduced by FAF, such as the T3 mobile AA and the HQ factories.
 - `Z_units.scd`: Contains data about the units introduced by FAF, such as the T3 mobile AA and the HQ factories.

If you'd want to load in other units make sure they are in the right folder structure. You can check `Z_units.lua` for the correct folder structure. This would allow you to convert a mod into a `.scd` file that the editor can then load in. 

For LOUD users you can just copy the `.scd` files that are part of your download.